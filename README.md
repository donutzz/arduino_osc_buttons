# Arduino_OSC_Buttons

Make an Arduino app that sends anOSC message over UDP (to Qlab) to trigger the start of a Cue
Written for the purpose to give a Livestream presenter the possibility to start Qlab cue's 

06-07-2020 Steven Geerts v1.0
Licensed under GNU GPLv3 (see COPYING)

make sure you adapt the Arduino IP, the Qlab IP, the target (Qlab) port and the Qlab Cue numbers. 
In this case it supports 5 buttons, but you cab add as many as your Arduino supports by copying and adapting the code. 
This code is tested and used on an Arduino One with a standard Arduino Ethernet shield. 

You need the OSCMessage component by Adrian Freed (many thanks for your great work!) 
This code is based on examples I found on the internet. If you feel like i did not respect your license, plezse let me know so I can adapt my code. 


